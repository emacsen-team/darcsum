---
title: darcsum
---

darcsum is an emacs add-on providing a more efficient user interface for
the darcs revision control system, in the style of pcl-cvs. It's great for
reviewing and selectively recording pending changes. The current
maintainer is Simon Michael <simon@joyful.com>.

## Download

 `darcs get --lazy http://joyful.com/repos/darcsum `

Add `-t .` to get the current released version, which is 1.2.

Here are the [latest changes](http://joyful.com/darcsweb/darcsweb.cgi?r=darcsum;a=shortlog),
including unreleased ones.

## Browse the code

 [http://joyful.com/darcsweb/darcsweb.cgi?r=darcsum;a=tree](http://joyful.com/darcsweb/darcsweb.cgi?r=darcsum;a=tree)

Some [developer notes](NOTES.org)

<img src="commits.png" />

## Related

- emacswiki pages: [DaRcs](http://www.emacswiki.org/emacs/DaRcs), [VersionControl](http://www.emacswiki.org/emacs/VersionControl)
- [vc-darcs.el](http://www.loveshack.ukfsn.org/emacs/) is another darcs ui for emacs ([emacswiki page](http://www.emacswiki.org/cgi-bin/wiki/vc-darcs.el))
- [darcs](http://darcs.net)
